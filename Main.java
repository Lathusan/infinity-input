package Problem5;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		// Read multi line input from console in Java by using two Scanners
		
		while (scanner.hasNextLine()) {
			List<String> tokens = new ArrayList<>();
			Scanner lineScanner = new Scanner(scanner.nextLine());

			while (lineScanner.hasNext()) {
				tokens.add(lineScanner.next());
			}

			lineScanner.close();
			System.out.println(tokens);
			System.out.println("");
		}

		scanner.close();
	}

	/*
	 * Scanner scan = new Scanner(System.in); String name = " "; 
	 * 	while (true) { 
	 * 	name = scan.nextLine(); 
	 * 		if (name.equals("")) { 
	 * 			break; 
	 * 		} 
	 * }
	 */

}